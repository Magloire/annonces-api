package com.mag.annonce.dto;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.Entity;


public class UserDto{

    private String firstName;
    private String lastName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
