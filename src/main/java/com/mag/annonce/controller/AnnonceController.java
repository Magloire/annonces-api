package com.mag.annonce.controller;

import com.mag.annonce.dto.AnnonceDto;
import com.mag.annonce.entity.Annonce;
import com.mag.annonce.service.IAnnonceService;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/api")
public class AnnonceController {

    @ConfigProperty(name = "frontend.url.redirect")
    String urlFrontRedirect;

    @Inject
    IAnnonceService annonceService;
    @GET
     @Path("/annonces")
    public List<AnnonceDto> getAllAnnonces() {
        return annonceService.getAllAnnonce();
    }

    @GET
    @Path("/liste")
    public Response redirectToListe(){
       return Response.status(302).header("Location",urlFrontRedirect).build();
    }
}
