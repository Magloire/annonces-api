package com.mag.annonce.service;

import com.mag.annonce.dto.AdresseDto;
import com.mag.annonce.dto.AnnonceDto;
import com.mag.annonce.dto.UserDto;
import com.mag.annonce.entity.Adresse;
import com.mag.annonce.entity.Annonce;
import com.mag.annonce.entity.User;

import javax.inject.Singleton;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Singleton
public class AnnonceServiceImpl implements IAnnonceService{
    @Override
    public List<AnnonceDto> getAllAnnonce() {

        List<Annonce> annoncesEntities = Annonce.listAll();
        List<AnnonceDto> annonceDtos = annoncesEntities.stream().
                map(annonceMapper::apply)
                .collect(Collectors.toList());

        return annonceDtos;
    }

    public Function<Annonce, AnnonceDto> annonceMapper = (annonce -> {
        AnnonceDto dto = new AnnonceDto();
        dto.setId(annonce.getId());
        dto.setTitle(annonce.getTitle());
        dto.setDateCreation(annonce.getDateCreation());
        dto.setType(annonce.getType());
        dto.setAdresseDto(adresseMapper.apply(annonce.getAdress()));
        dto.setCreator(userMapper.apply(annonce.getCreator()));
        return dto;
    });

    public static Function<Adresse, AdresseDto> adresseMapper = (adresse ->{
       AdresseDto dto = new AdresseDto();
       dto.setCommune(adresse.getCommune());
       dto.setCodePostal(adresse.getCodePostal());
       dto.setLibelleRue(adresse.getLibelleRue());
       dto.setNumeroRue(adresse.getNumeroRue());
       return dto;
    });

    public static  Function<User, UserDto> userMapper = (user ->{
        UserDto dto = new UserDto();
        dto.setFirstName(user.getFirstName());
        dto.setLastName(user.getLastName());
        return dto;
    });
}
