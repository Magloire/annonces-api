package com.mag.annonce.service;

import com.mag.annonce.dto.AnnonceDto;

import java.util.List;

public interface IAnnonceService {

    List<AnnonceDto> getAllAnnonce();
}
