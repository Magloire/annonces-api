package com.mag.annonce.entity;

public enum TypeAnnonce {
    IMMOBILIER, AMEUBLEMENT, SERVICE, AUTRES
}
